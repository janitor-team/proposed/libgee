/* task.c generated by valac 0.46.6, the Vala compiler
 * generated from task.vala, do not modify */

/* task.vala
 *
 * Copyright (C) 2013  Maciej Piechotka
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Maciej Piechotka <uzytkownik2@gmail.com>
 */

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <stdlib.h>
#include <string.h>

typedef gpointer (*GeeTask) (gpointer user_data);

#define GEE_TYPE_FUTURE (gee_future_get_type ())
#define GEE_FUTURE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GEE_TYPE_FUTURE, GeeFuture))
#define GEE_IS_FUTURE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEE_TYPE_FUTURE))
#define GEE_FUTURE_GET_INTERFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), GEE_TYPE_FUTURE, GeeFutureIface))

typedef struct _GeeFuture GeeFuture;
typedef struct _GeeFutureIface GeeFutureIface;
typedef gpointer (*GeeFutureMapFunc) (gconstpointer value, gpointer user_data);
typedef gconstpointer (*GeeFutureLightMapFunc) (gconstpointer value, gpointer user_data);
typedef gpointer (*GeeFutureZipFunc) (gconstpointer a, gconstpointer b, gpointer user_data);
typedef GeeFuture* (*GeeFutureFlatMapFunc) (gconstpointer value, gpointer user_data);
typedef struct _GeeTaskData GeeTaskData;

#define GEE_TYPE_PROMISE (gee_promise_get_type ())
#define GEE_PROMISE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GEE_TYPE_PROMISE, GeePromise))
#define GEE_PROMISE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GEE_TYPE_PROMISE, GeePromiseClass))
#define GEE_IS_PROMISE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEE_TYPE_PROMISE))
#define GEE_IS_PROMISE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GEE_TYPE_PROMISE))
#define GEE_PROMISE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GEE_TYPE_PROMISE, GeePromiseClass))

typedef struct _GeePromise GeePromise;
typedef struct _GeePromiseClass GeePromiseClass;
#define _gee_promise_unref0(var) ((var == NULL) ? NULL : (var = (gee_promise_unref (var), NULL)))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _gee_task_data_free0(var) ((var == NULL) ? NULL : (var = (gee_task_data_free (var), NULL)))
typedef struct _GeeAsyncTaskData GeeAsyncTaskData;
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_thread_pool_free0(var) ((var == NULL) ? NULL : (var = (g_thread_pool_free (var, FALSE, TRUE), NULL)))
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))

typedef enum  {
	GEE_FUTURE_ERROR_ABANDON_PROMISE,
	GEE_FUTURE_ERROR_EXCEPTION
} GeeFutureError;
#define GEE_FUTURE_ERROR gee_future_error_quark ()
struct _GeeFutureIface {
	GTypeInterface parent_iface;
	GType (*get_g_type) (GeeFuture* self);
	GBoxedCopyFunc (*get_g_dup_func) (GeeFuture* self);
	GDestroyNotify (*get_g_destroy_func) (GeeFuture* self);
	gconstpointer (*wait) (GeeFuture* self, GError** error);
	gboolean (*wait_until) (GeeFuture* self, gint64 end_time, gconstpointer* value, GError** error);
	void (*wait_async) (GeeFuture* self, GAsyncReadyCallback _callback_, gpointer _user_data_);
	gconstpointer (*wait_finish) (GeeFuture* self, GAsyncResult* _res_, GError** error);
	GeeFuture* (*map) (GeeFuture* self, GType a_type, GBoxedCopyFunc a_dup_func, GDestroyNotify a_destroy_func, GeeFutureMapFunc func, gpointer func_target, GDestroyNotify func_target_destroy_notify);
	GeeFuture* (*light_map) (GeeFuture* self, GType a_type, GBoxedCopyFunc a_dup_func, GDestroyNotify a_destroy_func, GeeFutureLightMapFunc func, gpointer func_target);
	GeeFuture* (*zip) (GeeFuture* self, GType a_type, GBoxedCopyFunc a_dup_func, GDestroyNotify a_destroy_func, GType b_type, GBoxedCopyFunc b_dup_func, GDestroyNotify b_destroy_func, GeeFutureZipFunc zip_func, gpointer zip_func_target, GeeFuture* second);
	GeeFuture* (*flat_map) (GeeFuture* self, GType a_type, GBoxedCopyFunc a_dup_func, GDestroyNotify a_destroy_func, GeeFutureFlatMapFunc func, gpointer func_target, GDestroyNotify func_target_destroy_notify);
	gconstpointer (*get_value) (GeeFuture* self);
	gboolean (*get_ready) (GeeFuture* self);
	GError* (*get_exception) (GeeFuture* self);
	GeeFuture* (*light_map_fixed) (GeeFuture* self, GType a_type, GBoxedCopyFunc a_dup_func, GDestroyNotify a_destroy_func, GeeFutureLightMapFunc func, gpointer func_target, GDestroyNotify func_target_destroy_notify);
};

struct _GeeTaskData {
	GeeTask function;
	gpointer function_target;
	GeePromise* promise;
};

struct _GeeAsyncTaskData {
	int _state_;
	GObject* _source_object_;
	GAsyncResult* _res_;
	GTask* _async_result;
	GeeFuture* _tmp0_;
	GeeFuture* _tmp1_;
	GError* _inner_error0_;
};

static GOnce gee_task_data_async_pool;
static GOnce gee_task_data_async_pool = G_ONCE_INIT;

GQuark gee_future_error_quark (void);
GType gee_future_get_type (void) G_GNUC_CONST;
GeeFuture* gee_task (GType g_type,
                     GBoxedCopyFunc g_dup_func,
                     GDestroyNotify g_destroy_func,
                     GeeTask task,
                     gpointer task_target,
                     GError** error);
G_GNUC_INTERNAL void gee_task_data_free (GeeTaskData * self);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GeeTaskData, gee_task_data_free)
G_GNUC_INTERNAL GeeTaskData* gee_task_data_new (void);
gpointer gee_promise_ref (gpointer instance);
void gee_promise_unref (gpointer instance);
GParamSpec* gee_param_spec_promise (const gchar* name,
                                    const gchar* nick,
                                    const gchar* blurb,
                                    GType object_type,
                                    GParamFlags flags);
void gee_value_set_promise (GValue* value,
                            gpointer v_object);
void gee_value_take_promise (GValue* value,
                             gpointer v_object);
gpointer gee_value_get_promise (const GValue* value);
GType gee_promise_get_type (void) G_GNUC_CONST;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GeePromise, gee_promise_unref)
GeePromise* gee_promise_new (GType g_type,
                             GBoxedCopyFunc g_dup_func,
                             GDestroyNotify g_destroy_func);
GeePromise* gee_promise_construct (GType object_type,
                                   GType g_type,
                                   GBoxedCopyFunc g_dup_func,
                                   GDestroyNotify g_destroy_func);
GeeFuture* gee_promise_get_future (GeePromise* self);
G_GNUC_INTERNAL GThreadPool* gee_task_data_get_async_pool (void);
static void gee_async_task_data_free (gpointer _data);
void gee_async_task (GAsyncReadyCallback _callback_,
                     gpointer _user_data_);
void gee_async_task_finish (GAsyncResult* _res_,
                            GError** error);
static gboolean gee_async_task_co (GeeAsyncTaskData* _data_);
static gpointer _gee_async_task_co_gee_task (gpointer self);
static void gee_task_data_instance_init (GeeTaskData * self);
G_GNUC_INTERNAL void gee_task_data_run (GeeTaskData* self);
void gee_promise_set_value (GeePromise* self,
                            gpointer value);
static GThreadPool* __lambda58_ (void);
static void ___lambda59_ (GeeTaskData* tdata);
static void ____lambda59__gfunc (gpointer data,
                          gpointer self);
static gpointer ___lambda58__gthread_func (gpointer self);

/**
 * Schedules a task to execute asynchroniously. Internally one
 * of threads from pool will execute the task.
 *
 * Note: There is limited number of threads unless environment variable
 *   ``GEE_NUM_THREADS`` is set to -1. It is not adviced to call I/O or
 *   block inside the taks. If necessary it is possible to create a new one
 *   by anyther call.
 *
 * @param task Task to be executed
 * @return Future value returned by task
 * @see async_task
 * @since 0.11.0
 */
static gpointer
_g_object_ref0 (gpointer self)
{
	return self ? g_object_ref (self) : NULL;
}

GeeFuture*
gee_task (GType g_type,
          GBoxedCopyFunc g_dup_func,
          GDestroyNotify g_destroy_func,
          GeeTask task,
          gpointer task_target,
          GError** error)
{
	GeeTaskData* tdata = NULL;
	GeeTaskData* _tmp0_;
	GeeTaskData* _tmp1_;
	GeeTask _tmp2_;
	gpointer _tmp2__target;
	GeeTaskData* _tmp3_;
	GeePromise* _tmp4_;
	GeeFuture* _result_ = NULL;
	GeeTaskData* _tmp5_;
	GeePromise* _tmp6_;
	GeeFuture* _tmp7_;
	GeeFuture* _tmp8_;
	GeeFuture* _tmp9_;
	GThreadPool* _tmp10_;
	GeeTaskData* _tmp11_;
	GError* _inner_error0_ = NULL;
	GeeFuture* result = NULL;
	_tmp0_ = gee_task_data_new ();
	tdata = _tmp0_;
	_tmp1_ = tdata;
	_tmp2_ = task;
	_tmp2__target = task_target;
	task = NULL;
	task_target = NULL;
	_tmp1_->function = _tmp2_;
	_tmp1_->function_target = _tmp2__target;
	_tmp3_ = tdata;
	_tmp4_ = gee_promise_new (g_type, (GBoxedCopyFunc) g_dup_func, (GDestroyNotify) g_destroy_func);
	_gee_promise_unref0 (_tmp3_->promise);
	_tmp3_->promise = _tmp4_;
	_tmp5_ = tdata;
	_tmp6_ = _tmp5_->promise;
	_tmp7_ = gee_promise_get_future (_tmp6_);
	_tmp8_ = _tmp7_;
	_tmp9_ = _g_object_ref0 (_tmp8_);
	_result_ = _tmp9_;
	_tmp10_ = gee_task_data_get_async_pool ();
	_tmp11_ = tdata;
	tdata = NULL;
	g_thread_pool_push (_tmp10_, _tmp11_, &_inner_error0_);
	if (G_UNLIKELY (_inner_error0_ != NULL)) {
		if (_inner_error0_->domain == G_THREAD_ERROR) {
			g_propagate_error (error, _inner_error0_);
			_g_object_unref0 (_result_);
			_gee_task_data_free0 (tdata);
			return NULL;
		} else {
			_g_object_unref0 (_result_);
			_gee_task_data_free0 (tdata);
			g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
			g_clear_error (&_inner_error0_);
			return NULL;
		}
	}
	result = _result_;
	_gee_task_data_free0 (tdata);
	return result;
}

static void
gee_async_task_data_free (gpointer _data)
{
	GeeAsyncTaskData* _data_;
	_data_ = _data;
	g_slice_free (GeeAsyncTaskData, _data_);
}

void
gee_async_task (GAsyncReadyCallback _callback_,
                gpointer _user_data_)
{
	GeeAsyncTaskData* _data_;
	_data_ = g_slice_new0 (GeeAsyncTaskData);
	_data_->_async_result = g_task_new (NULL, NULL, _callback_, _user_data_);
	g_task_set_task_data (_data_->_async_result, _data_, gee_async_task_data_free);
	gee_async_task_co (_data_);
}

void
gee_async_task_finish (GAsyncResult* _res_,
                       GError** error)
{
	GeeAsyncTaskData* _data_;
	_data_ = g_task_propagate_pointer (G_TASK (_res_), error);
	if (NULL == _data_) {
		return;
	}
}

/**
 * Continues the execution asynchroniously in helper thread. Internally
 * one of threads from pool will execute the task.
 *
 * Note: There is limited number of threads unless environment variable
 *   ``GEE_NUM_THREADS`` is set to -1. It is not adviced to call I/O or
 *   block inside the taks. If necessary it is possible to create a new one
 *   by anyther call.
 *
 * @see task
 * @since 0.11.0
 */
static gpointer
_gee_async_task_co_gee_task (gpointer self)
{
	gpointer result;
	result = (gpointer) ((gintptr) gee_async_task_co (self));
	return result;
}

static gboolean
gee_async_task_co (GeeAsyncTaskData* _data_)
{
	switch (_data_->_state_) {
		case 0:
		goto _state_0;
		default:
		g_assert_not_reached ();
	}
	_state_0:
	_data_->_tmp0_ = gee_task (G_TYPE_BOOLEAN, NULL, NULL, _gee_async_task_co_gee_task, _data_, &_data_->_inner_error0_);
	_data_->_tmp1_ = _data_->_tmp0_;
	_g_object_unref0 (_data_->_tmp1_);
	if (G_UNLIKELY (_data_->_inner_error0_ != NULL)) {
		if (_data_->_inner_error0_->domain == G_THREAD_ERROR) {
			g_task_return_error (_data_->_async_result, _data_->_inner_error0_);
			g_object_unref (_data_->_async_result);
			return FALSE;
		} else {
			g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _data_->_inner_error0_->message, g_quark_to_string (_data_->_inner_error0_->domain), _data_->_inner_error0_->code);
			g_clear_error (&_data_->_inner_error0_);
			g_object_unref (_data_->_async_result);
			return FALSE;
		}
	}
	g_task_return_pointer (_data_->_async_result, _data_, NULL);
	if (_data_->_state_ != 0) {
		while (!g_task_get_completed (_data_->_async_result)) {
			g_main_context_iteration (g_task_get_context (_data_->_async_result), TRUE);
		}
	}
	g_object_unref (_data_->_async_result);
	return FALSE;
}

G_GNUC_INTERNAL void
gee_task_data_run (GeeTaskData* self)
{
	GeePromise* _tmp0_;
	GeeTask _tmp1_;
	gpointer _tmp1__target;
	gpointer _tmp2_;
	g_return_if_fail (self != NULL);
	_tmp0_ = self->promise;
	_tmp1_ = self->function;
	_tmp1__target = self->function_target;
	_tmp2_ = _tmp1_ (_tmp1__target);
	gee_promise_set_value (_tmp0_, _tmp2_);
}

static gboolean
int64_try_parse (const gchar* str,
                 gint64* _result_,
                 const gchar* * unparsed)
{
	gint64 _vala__result_ = 0LL;
	const gchar* _vala_unparsed = NULL;
	gchar* endptr = NULL;
	gchar* _tmp0_ = NULL;
	gint64 _tmp1_;
	gchar* _tmp2_;
	gint _tmp3_;
	gint _tmp4_;
	gboolean result = FALSE;
	g_return_val_if_fail (str != NULL, FALSE);
	_tmp1_ = g_ascii_strtoll (str, &_tmp0_, (guint) 0);
	endptr = _tmp0_;
	_vala__result_ = _tmp1_;
	_tmp2_ = endptr;
	_tmp3_ = strlen (str);
	_tmp4_ = _tmp3_;
	if (_tmp2_ == (((gchar*) str) + _tmp4_)) {
		_vala_unparsed = "";
		result = TRUE;
		if (_result_) {
			*_result_ = _vala__result_;
		}
		if (unparsed) {
			*unparsed = _vala_unparsed;
		}
		return result;
	} else {
		gchar* _tmp5_;
		_tmp5_ = endptr;
		_vala_unparsed = (const gchar*) _tmp5_;
		result = FALSE;
		if (_result_) {
			*_result_ = _vala__result_;
		}
		if (unparsed) {
			*unparsed = _vala_unparsed;
		}
		return result;
	}
	if (_result_) {
		*_result_ = _vala__result_;
	}
	if (unparsed) {
		*unparsed = _vala_unparsed;
	}
}

static void
___lambda59_ (GeeTaskData* tdata)
{
	g_return_if_fail (tdata != NULL);
	gee_task_data_run (tdata);
	_gee_task_data_free0 (tdata);
}

static void
____lambda59__gfunc (gpointer data,
                     gpointer self)
{
	___lambda59_ ((GeeTaskData*) data);
}

static GThreadPool*
__lambda58_ (void)
{
	gint num_threads = 0;
	gchar* gee_num_threads_str = NULL;
	const gchar* _tmp0_;
	gchar* _tmp1_;
	const gchar* _tmp2_;
	GError* _inner_error0_ = NULL;
	GThreadPool* result = NULL;
	num_threads = (gint) g_get_num_processors ();
	_tmp0_ = g_getenv ("GEE_NUM_THREADS");
	_tmp1_ = g_strdup (_tmp0_);
	gee_num_threads_str = _tmp1_;
	_tmp2_ = gee_num_threads_str;
	if (_tmp2_ != NULL) {
		gint64 _result_ = 0LL;
		const gchar* _tmp3_;
		gint64 _tmp4_ = 0LL;
		gboolean _tmp5_;
		_tmp3_ = gee_num_threads_str;
		_tmp5_ = int64_try_parse (_tmp3_, &_tmp4_, NULL);
		_result_ = _tmp4_;
		if (_tmp5_) {
			num_threads = (gint) _result_;
		}
	}
	{
		GThreadPool* _tmp6_ = NULL;
		GThreadPool* _tmp7_;
		GThreadPool* _tmp8_;
		_tmp7_ = g_thread_pool_new (____lambda59__gfunc, NULL, num_threads, FALSE, &_inner_error0_);
		_tmp6_ = _tmp7_;
		if (G_UNLIKELY (_inner_error0_ != NULL)) {
			if (_inner_error0_->domain == G_THREAD_ERROR) {
				goto __catch4_g_thread_error;
			}
			_g_free0 (gee_num_threads_str);
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
			g_clear_error (&_inner_error0_);
			return NULL;
		}
		_tmp8_ = _tmp6_;
		_tmp6_ = NULL;
		result = _tmp8_;
		_g_thread_pool_free0 (_tmp6_);
		_g_free0 (gee_num_threads_str);
		return result;
	}
	goto __finally4;
	__catch4_g_thread_error:
	{
		GError* err = NULL;
		err = _inner_error0_;
		_inner_error0_ = NULL;
		abort ();
		_g_error_free0 (err);
	}
	__finally4:
	_g_free0 (gee_num_threads_str);
	g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error0_->message, g_quark_to_string (_inner_error0_->domain), _inner_error0_->code);
	g_clear_error (&_inner_error0_);
	return NULL;
}

static gpointer
___lambda58__gthread_func (gpointer self)
{
	gpointer result;
	result = __lambda58_ ();
	return result;
}

G_GNUC_INTERNAL GThreadPool*
gee_task_data_get_async_pool (void)
{
	gconstpointer _tmp0_;
	GThreadPool* result = NULL;
	_tmp0_ = g_once (&gee_task_data_async_pool, ___lambda58__gthread_func, NULL);
	result = (GThreadPool*) _tmp0_;
	return result;
}

G_GNUC_INTERNAL GeeTaskData*
gee_task_data_new (void)
{
	GeeTaskData* self;
	self = g_slice_new0 (GeeTaskData);
	gee_task_data_instance_init (self);
	return self;
}

static void
gee_task_data_instance_init (GeeTaskData * self)
{
}

G_GNUC_INTERNAL void
gee_task_data_free (GeeTaskData * self)
{
	_gee_promise_unref0 (self->promise);
	g_slice_free (GeeTaskData, self);
}

